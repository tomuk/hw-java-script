/*
Создайте приложение секундомер.
            * Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
            * При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый
            * Вывод счётчиков в формате ЧЧ:ММ:СС
            * Реализуйте Задание используя синтаксис ES6 и стрелочные функции
*/
//Timer 
const hoursElement = document.querySelector('.hours');
const minutesElement = document.querySelector('.minutes');
const secondsElement = document.querySelector('.seconds');

//Bottons
const startButton = document.querySelector('.start');
const stopButton = document.querySelector('.stop');
const recevetButton = document.querySelector('.reset');
const display = document.querySelector('.display');

let hour = 0,
    minute = 0,
    second = 0,
    interval;


startButton.onclick = () => {
   
    clearInterval(interval);
    interval = setInterval(startTimer, 1000);
    display.style.backgroundColor = "rgba(39, 211, 76, 0.5)";
};
stopButton.onclick = () => {
    clearInterval(interval)
    display.style.backgroundColor = "rgba(231, 40, 40, 0.5";

};
recevetButton.onclick = () => {
    clearInterval(interval)
    hour = 0,
    minute = 0,
    second = 0;
    hoursElement.textContent = "00"
    minutesElement.textContent = "00"
    secondsElement.textContent = "00"
    display.style.backgroundColor = "rgba(88, 88, 88, .5)";

};

const startTimer = ()  => {
   
    second++
  
    if (second < 9) {
        secondsElement.innerText = "0" + second
    }
    if (second > 9) {
        secondsElement.innerText = second 
    }
    if (second > 59) {
        minute++
        minutesElement.innerText = "0" + minute
        second = 0
        secondsElement.innerText = "0" + second
    }


 //Minute
    if (minute < 9) {
        minutesElement.innerText = "0" + minute
    };
    if (minute> 9) {
        minutesElement.innerText = minute
    };
    if (minute > 59) {
        hour++
        hoursElement.innerText = "0" + minute
        minute = 0
        second = 0
        secondsElement.innerText = "0" + second
    }

    //Hours
    if (hour < 9) {
        hoursElement.innerText = "0" + hour
    };
    if (hour > 9) {
        hoursElement.innerText = hour
    };
    
    
    
    
}