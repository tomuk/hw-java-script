/*
Слайдер
        Создайте слайдер который каждые 3 сек будет менять изображения 
        Изображения для отображения
        https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
        https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
        https://naukatv.ru/upload/files/shutterstock_418733752.jpg
        https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg          https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
*/

window.addEventListener("DOMContentLoaded", () => {
        const img = document.createElement('img');
        img.className = 'imgStyle';
        img.style.width = '600px';
        img.style.height = '300px';
        img.style.border = "2px solid red ";
        img.src = "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg"
        document.body.prepend(img);

        const arr = [
                "https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
                "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
                "https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
                "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg"
        ];

        let index = 0;
        
        setInterval(() => {
                img.src = arr[index++ % arr.length];
    
        }, 3000);
})