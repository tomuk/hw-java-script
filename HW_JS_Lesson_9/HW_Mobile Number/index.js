/*
 Реализуйте программу проверки телефона<br>
* Используя JS Создайте поле для ввода телефона и кнопку сохранения
* Пользователь должен ввести номер телефона в формате 000-000-00-00 

* Поле того как пользователь нажимает кнопку сохранить проверте правильность ввода номера, если номер правильный.
сделайте зеленый фон и используя document.location переведите пользователя на страницу https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg.
если будет ошибка отобразите её в диве до input.
*/


window.addEventListener("DOMContentLoaded", function () {
    const head = document.querySelector('div');
    head.style.width = '400px';
    head.style.border = '2px solid red';
    head.style.padding = '10px';


    const msg = document.querySelector('#message');
    const click = document.getElementById("seved");
    
    click.onclick = (e) => {
        let number = /\d{3}-\d{3}-\d{2}-\d{2}/;
        let myPhone = document.querySelector('#phone').value
        let valid = number.test(myPhone)
        if (valid) {
            head.style.backgroundColor = 'green';
            setTimeout(() => {
                document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg'

            },2000)
        }
        else if (myPhone == "") { 
            let div = document.createElement('div');
            msg.style.color = 'red'; 
            msg.innerHTML = 'Ви зберегли пусте поле!'
            setTimeout(() => {
                msg.innerHTML = 'Введіть свій мобільний номер у форматі (000-000-00-00)'
                msg.style.color = 'black';
            },2000)
          
        }
        else {
            let div = document.createElement('div');
            msg.style.color = 'red';
            msg.innerHTML = 'Номер введено неправильно!'
            setTimeout(() => {
                msg.innerHTML = 'Введіть свій мобільний номер у форматі (000-000-00-00)'
                msg.style.color = 'black';
            },2000)
        }
        return valid;
    
    }

})