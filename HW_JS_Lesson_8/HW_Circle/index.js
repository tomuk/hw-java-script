//Завдання 1
//При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
//При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.


const createNewElements = () => {


  function creativeCircle() {
    const diam = Number(prompt('Введіть діаметр круга', '100'));

    const div = document.createElement('div');
    div.className = "divCircle";
    div.style.width = diam * 10 + `px`;
    document.body.append(div)

    for (let i = 1; i <= 100; i++) {
      let circle = document.createElement('div')
      circle.style.display = `inline-block`
      circle.style.border = '1px solid black';
      circle.style.borderRadius = `50%`;
      circle.style.height = diam + `px`;
      circle.style.width = diam - 2 + `px`;
      circle.style.cursor = `pointer`;
      circle.className = 'newCircle';
      const randomColor = Math.floor(Math.random() * 16777215).toString(16);
      circle.style.backgroundColor = "#" + randomColor;
      div.append(circle)
    }
  }
  creativeCircle();
  
  let myCircle = document.querySelectorAll('.newCircle');
	
	myCircle.forEach(circ => circ.addEventListener('click', removeBlock));
	
	function removeBlock() {
	  let circ = this;
	  
	  let circlId = setInterval(function() {
		  clearInterval(circlId);
		  circ.remove();
		
	  }, 60)
	}
}
