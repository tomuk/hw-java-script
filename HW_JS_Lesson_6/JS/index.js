//Завдання 1:
//Разработайте функцию-конструкто, которая будет создавать обьект Human(человек). Создайте массив обьектов и реализуйте функцию, которая будет сортировать элементы массива по значению свойств Age по возрастанию или по убыванию. */
document.write(`<h3> Завдання 1 </h3><hr>`)


function Human(name,surname,age){
    this.name = name;
    this.surname = surname;
    this.age = age;
}
Human.prototype.sortHuman = function(){
    document.write( ` Учасник - ${this.name} ${this.surname}, ${this.age} років. <br>`)

}
const Ivan = new Human("Ivan","Ivanov", 78);
Ivan.sortHuman();
const Alexander = new Human("Alexander","Petrov", 18);
Alexander.sortHuman();
const Dmitry = new Human("Dmitry","Kurchatov", 23);
Dmitry.sortHuman();
const Andy = new Human("Andy","Huka", 35);
Andy .sortHuman();
const Jack = new Human("Jack","Lokk", 56);
Jack.sortHuman();

const man = [Ivan, Alexander, Dmitry,Andy,Jack ];
man.sort((a, b) => a.age - b.age);
document.write(`<br><b> Анкета за зростанням віку (): </b><br>  `)
let i = 1;
for (iteam of man){
    document.write(`<br> Ім'я: ${iteam.name} <br>Прізвище: ${ iteam.surname}  <br>Вік: ${iteam.age}  <br>`)
    i++
}

/*
Задание 2:
Разработайте функцию-конструктор, которая будет создавать обьект Human(человек), добавьте на свое усмотрение свойства и методы в этот обьект. Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какие уровня функции-конструктора.
*/
document.write(`<h3> Завдання 2 </h3><hr>`)


class Humann {
    constructor(name,surname,age,country,city){
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.country = country;
        this.city = city;


    }
    infoHuman(){
        this.name = prompt("Ваше ім'я",'Ivan');
        this.surname = prompt("Ваше прізвище",'Ivanov');
        this.age = Number(prompt("Ваш вік",`35`));
        this.country = prompt("Країна в якій проживаєте",`Ukraine`);
        this.city = prompt("Місто в якому проживаєте",`Kyiv`);
        
    }
    writeHuman(){
     document.write(`<br> Ваше ім'я - ${this.name} <br> Ваше прізвище - ${this.surname}<br>Ваш вік - ${this.age}<br>Країна в якій проживаєте - ${this.country} <br>Місто в якому проживаєте - ${this.city}<br>`)
    }
    countoOject = () => {
        Humann.count +=1
    }
}
Humann.newName = function () {
    return new Humann(`Petro`,`Petrov`,`50`,`Ukraine`,`Ryzhun`);
}

const human1 = new Humann ();
human1.infoHuman();
human1. writeHuman();


const human2 = new Humann ();
human2.infoHuman();
human2. writeHuman();


Humann.newName().writeHuman();

