//Завдання із Class Work Lesson 4 (Cтворіть Калькулятор (ljhj,sns))
/*
 За каждую операцию будет отвечать отдельная функция,
 т.е. для сложения - add(a,b), для умножения - multiple(a,b) и т.д.
Каждая из них принимает в аргументы только два числа и возвращает результат операции над двумя числами
Если число не передано в функцию аргументом - ПО УМОЛЧАНИЮ присваивать этому аргументу 0.
Основная функция calculate()
Принимает ТРИ АРГУМЕНТА:
1 - число
2 - число
3 - функция которую нужно выполнить для двух этих чисел.
Таким образом получается что основная функция калькулятор будет вызывать переданную ей аргументом функцию для двух чисел, которые передаются остальными двумя аргументами. При делении на 0 выводить ошибку.
Функия калькулятор доджна принять на вход 3 аругмента, Если аргументов больше или меньше выводить ошибку.         
*/
document.write("<b>" + "Калькулятор"+ "</b>" + "</br>");
function calculator (first,second,callback){
    return (callback(first,second));
}
const add = (a=0,b=0) =>{
    return a+b;
}
const mult = (a=0,b=0) =>{
    return a*b;
}
const sub = (a=0,b=0) =>{
    return a-b;
}
const div = (a=0,b=0) =>{
    if (a === 0 || b === 0){

        document.write('<h1>'+"Деление на ноль нельзя "+'</h1>')
    }
    else return a / b;
}
function show(msg){
    document.write(`</br>  Результат = <b> ${msg}</b>`);
}
function calc(operand1, operand2, sign) {
    if(arguments.length < 3 || arguments.length > 3) {
        return console.error("В функцию можно передать только 3 аргумента.");
    }
    
}
function setDate(){
    let operand1 = parseInt(prompt('input first number'));
    let operand2 = parseInt(prompt('input second number'));
    let sign = prompt('Введите знак');
    switch(sign){
        case '+' :  
        show(calculator(operand1,operand2,add ));
        break;
        case '-' :  
        show(calculator(operand1,operand2,sub ));
        break;
        case '*' :  
        show(calculator(operand1,operand2,mult));
        break;
        case '/' :  
        show(calculator(operand1,operand2,div));
        break;
        default:
        document.write("<p><b style='color:red'>" + sign + "</b> - не является знаком арифметической операции.");
        }

};

  setDate();
  

document.write("</br><hr>");

                        // HOMEWOKR Lesson 4
// Завдання 1
/*
Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.
*/
document.write("<b>" + "Завдання 1 "+ "</b>" + "</br>");
document.write("</br>");

const arr = [1,2,3,4,5,6,7,8,9];
function fn(someArray){
    let newArr = [];
    for(let i = 0; i < someArray.length; i++){
        newArr[i] = someArray[i] + 1;
    }
    return newArr ;
}
 function map(array , fun) {
    document.write(fun(array));
    }

map(arr,fn);



 document.write("</br><hr>");

//Завдання 2
/*
Перепишите функцию, используя оператор '?' или '||'         
Следующая функция возвращает true, если параметр age больше 18.
В ином случае она задаёт вопрос confirm и возвращает его результат. 
1 --function checkAge(age)
2 -- if (age > 18)
3 -- return true;
4 -- } else {
5 --  return confirm('Родители разрешили?');
6 --   } }
</tbody>
*/
document.write("<b>" + "Завдання 2 "+ "</b>" + "</br>");
document.write("Перевірка в консолі" + "</br>");
function checkAge(age){
    return age > 18 ? true : confirm('Родители разрешили?');
}
/* Варіант з ||
function checkAge(age) {
    return (age > 18) || confirm('Родители разрешили?');
  }
  */
  console.log(checkAge(15));
  

