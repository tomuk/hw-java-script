//Задача 1
var x = 6;
var y = 14;
var z = 4;

x += y - x++ * z; // x = (x +(y - (x++ * z))) = (6 + (14 - (6++ * 4))) = -4
/* 
1 (x++) ->  x = 6
2 (6 * z) -> (6 * 4 = 24)
3 (y - 24) -> (14 - 24 = -10)
4 ( x +(-10)) -> (6 + (-10) = -4)
*/
document.write("x += y - x++ * z = " + "<span><b>" + x + "</b></span>" +"<br/><hr/>" );
console.log(x);


//Задача 2
x = 6;
y = 14;
z = 4;

z = --x - y * 5; // z = --x - y * 5 = (6-1) + (- 14 * 5) = -65
/*
1 (--x) -> (6 - 1 = 5)
2 (y * 5) -> (14 * 5 = 70)
3 (--x - y * 5) -> (5 - 70 = -65)
*/
document.write("z = --x - y * 5 = " + "<span><b>" + z + "</b></span>" +"<br/><hr/>" );
console.log(z);


//Задача 3
x = 6;
y = 14;
z = 4;

y /= x + 5 % z; // y = (y / (x + 5 % z)) = (14 / (6 + 5 % 4 )) = 2
/*
1 (5 % z) -> (5 % 4 = 1)
2 (x + 1) -> (6 + 1 = 7)
3 (y / 7) -> (14 / 7 = 2)
*/
document.write("y /= x + 5 % z = " + "<span><b>" + y + "</b></span>" +"<br/><hr/>" );
console.log(y);



//Задача 4
x = 6;
y = 14;
z = 4;

var result;
result = z - x++ + y * 5; // result = 4 - 6 + 14 * 5 = -2 + 70 = 68
/*
1 (y * 5) -> (14 * 5 = 70)
2 (x++) -> (x = 6)
3 (z - 6 + 70) -> (4 - 6 + 70 = 68)
*/
document.write("z - x++ + y * 5 = " + "<span><b>" + result + "</b></span>" +"<br/><hr/>" );
console.log(result);



//Задача 5
x = 6;
y = 14;
z = 4;

x = y - x++ * z; // x = 14 - 6 * 4 = -10
/*
1 (x++) -> (x = 6)
2 (6 * z) -> (6 * 4 = 24)
3 (y - 24) -> (14 - 24 = -10)
*/
document.write("x = y - x++ * z = " + "<span><b>" + x + "</b></span>" +"<br/><hr/>" );
console.log(x);

